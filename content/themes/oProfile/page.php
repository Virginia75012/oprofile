<?php get_header(); ?>

<?php if (have_posts()) : while(have_posts()): the_post(); ?>
<section class="banner" style="background-image: url(<?= get_the_post_thumbnail_url(); ?>)">
        <div class="intro">
            <h1 class="intro__title"><?php the_title(); ?></h1>
                <div class="intro__content">
                    <?php the_content(); ?>
                </div>
            </div>
        </section>
    <?php endwhile; endif; ?>

    <?php get_footer(); ?>