<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <?php wp_head(); ?>

    </head>
    <body>

        <div class="wrapper">

            <header class="header">
                <div class="logo">
                    <a href="#">oProfile</a>
                </div>
                <div class="social-nav">
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-google-plus"></i></a>
                    <a href="#"><i class="fa fa-instagram"></i></a>
                    <a href="#"><i class="fa fa-github"></i></a>
                </div>
                <a href="#" class="ui-button"><i class="fa fa-bars"></i></a>
            </header>