<div class="grid__item">
    <i class="grid__item__icon fa fa-user"></i>
        <h3 class="grid__item__title"><?php the_title(); ?></h3>
            <p class="grid__item__content"><?php the_content(); ?></p>
</div>