<section class="grid">

<?php
    $args = [
        'post_type' => 'post',
        'category_name' => 'competence',
        'posts_per_page' => -1,
        'order' => 'ASC',
        'orderby' => 'title'
    ];

    $nos_competences = new WP_Query($args);

    if ($nos_competences->have_posts()): while ($nos_competences->have_posts()): $nos_competences->the_post();
        get_template_part('template-parts/front-page/grid', 'item');
    endwhile;
    // Je remet les variables globals telle qu'elles étaient avant ma boucle custom
    wp_reset_postdata();

    endif; ?>

    </section>
