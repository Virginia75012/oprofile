<section class="posts" id="posts">
    <?php
    $args = [
        'post_type' => 'post',
        'category_name' => 'blog',
        'posts_per_page' => get_theme_mod('oprofile_theme_posts_numbers', 6),
        'order' => 'DESC',
        'orderby' => 'date'
    ];

    $nos_articles = new WP_Query($args);

    if ($nos_articles->have_posts()): while ($nos_articles->have_posts()): $nos_articles->the_post();
        get_template_part('template-parts/front-page/post');
    endwhile;
    // Je remet les variables globals telle qu'elles étaient avant ma boucle custom
    wp_reset_postdata();

    endif; ?>
</section>