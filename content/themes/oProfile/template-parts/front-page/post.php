<article class="post" style="background-image: url(<?= get_the_post_thumbnail_url(); ?>);">
    <h2 class="post__title">
    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
    </h2>
    <div class="post__content">
        <?php the_excerpt(); ?>
    </div>
</article>