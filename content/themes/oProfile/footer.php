<footer class="footer" id="contact">
        <div class="contact-form">
            <form action="">
                <div class="field is-half">
                    <label for="name">Nom</label>
                        <input type="text" name="name" id="name" />
                </div>
                <div class="field is-half">
                    <label for="email">Email</label>
                        <input type="email" name="email" id="email" />
                </div>
                <div class="field">
                    <label for="message">Message</label>
                        <textarea name="message" id="message"></textarea>
                </div>
                <div class="field">
                    <input type="submit" value="Envoyer">
                </div>
            </form>
        </div>

        <div class="contact-info">
            <div class="contact-info__part">
                <i class="fa fa-envelope"></i>
                    <h4>Email</h4>
                        <a href="mailto:christophe@oclock.io">christophe@oclock.io</a>
            </div>
            <div class="contact-info__part">
                <i class="fa fa-phone"></i>
                    <h4>Téléphone</h4>
                        <a href="tel:+33604025351">06.04.02.53.51</a>
            </div>

            <div class="contact-info__part">
                <i class="fa fa-home"></i>
                    <h4>Adresse</h4>
                        <p>223 allée de la gare<br />4400 Nantes</p>
            </div>
        </div>
    </footer>
</div>

    <?php get_template_part('template-parts/footer/menu', 'main'); ?>

    <?php wp_footer(); ?>

</body>
</html>