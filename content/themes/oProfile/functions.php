<?php

function oprofile_scripts(){

    //Appel de la fonction wp_enqueue_style
    //Permet à WP de déclarer des feuilles de style
    wp_enqueue_style(
        'oprofile-app-css',
        get_theme_file_uri('public/css/app.css'),
        ['oprofile-vendors-css'],
        '1.0.0'
    );

    wp_enqueue_style(
        'oprofile-vendors-css',
        get_theme_file_uri('public/css/vendors.css'),
        [],
        '1.0.0'
    );

    //Appel de la fonction wp_enqueue_style
    //Permet à WP de déclarer des feuilles de script
    wp_enqueue_script(
        'oprofile-app-js',
        get_theme_file_uri('public/js/app.js'),
        ['oprofile-vendors-js'],
        '1.0.0',
        true
    );

    wp_enqueue_script(
        'orprofile-vendors-js',
        get_theme_file_uri('public/js/vendors.js'),
        [],
        '1.0.0',
        true
    );
}
add_action('wp_enqueue_scripts', 'oprofile_scripts');

function oprofile_setup(){

    //Demande à WP de générer la balise "title"
    add_theme_support('title-tag');

    //Déclare à WP l'existence de menu au sein de mon thème
    register_nav_menus([
        'main'=>'Menu affiché avec le bouton',
        'social'=>'Menu réseau sociaux'
    ]);

    //Déclare à WP que mon thème gère les images
    add_theme_support('post-thumbnails');

    //Masque la barre admin sur mon thème (masque le menu)
    show_admin_bar(false);
}
add_action('after_setup_theme', 'oprofile_setup');

