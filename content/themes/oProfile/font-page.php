<?php get_header(); ?>

<?php

//J'affiche le contenu "principal" de la page
//Autrement dit, ce qui était défini le BO
//comme étant le contenu à afficher sur la page d'accueil

if (have_posts()): while(have_posts()): the_post();

    get_template_part('template-parts/front-page/banner');

endwhile; endif;

get_template_part('template-parts/front-page/posts');

get_template_part('template-parts/front-page/grid');

get_footer(); ?>