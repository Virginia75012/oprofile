var app = {

  init: function(){

    console.log('app.init');

    //Je crée mes variables pour cibler mes éléments plus facilement
    $body = $('body');
    $header = $('.header');
    $banner = $('.banner');

    //Je cible tous les a dont le href vaut #
    //Mais pas ceux dont le href ne vaut que #
    $('a[href*="#"]:not([href="#"])').on('click', app.smoothScroll);

    $('.ui-button').on('click', app.toogleMenu);

    //Mise en place de scrollex
    $banner.scrollex({

      bottom: $header.height(),

      enter: function(){
        $header.removeClass('fixed');
      },
      leave: function(){
        $header.addClass('fixed');
      }

    });
  },

  toogleMenu: function(evt){
    console.log('toogleMenu');

    //Je supprime l'événement par défaut du bouton
    event.preventDefault();

    $body.toggleClass('menu-visible');
  },

  smoothScroll: function(evt){

    //Je supprime l'événement par défaut de l'ancre
    evt.preventDefault();

    //Je transforme la cible en objet jQuery
    var $target = $(this.hash);

    //Si l'élément existe bien...
    if($target.length){

      var targetPosition = $target.offset().top - $header.height() + 1;

      $('html, body').animate({
        scrollTop: targetPosition
      }, 1500);
    }

  }
}

$(app.init);